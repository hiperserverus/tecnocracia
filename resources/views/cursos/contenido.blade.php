@extends('layouts.app')

@section('header')

@section('header')

@include('partials.header_content')

@endsection

@endsection

@section('title',$curso_name)

@section('content')

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.8&appId=396802444017789";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    <div class='course-sidebar'>
  <!-- Image & Title -->
  <img class='course-image' src="{{url('/images/cursos/')}}/{{$imagen->nombre }}">
  <h2>{{$curso_name}}</h2>
  <!-- Course Progress -->
  <div class='course-progress'>
    <div class='progressbar'>
      <div class='progressbar-fill' style='min-width: {{$porcentaje_curso}}%;'></div>
    </div>
    <div class='small course-progress'>
      <span class='percentage'>
        {{$porcentaje_curso}}%
      </span>
      COMPLETO
    </div>
  </div>
  <!-- Sidebar Nav -->
  <ul class='sidebar-nav'>
    
      <li class="selected">
        <a href='/courses/61310'>
          <span class='lecture-sidebar-icon'>
            <i class='fa fa-key-alt'></i>
          </span>
         <i class="fa fa-unlock-alt" aria-hidden="true"></i>Contraseña: Vekingo.com
        </a>
      </li>

            <div class="fb-page" data-href="https://www.facebook.com/vekingo/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
            <blockquote cite="https://www.facebook.com/vekingo/" class="fb-xfbml-parse-ignore">
                  <a href="https://www.facebook.com/vekingo/">Vekingo</a>
           </blockquote>
    </div>
    
    <li class="">
      <a href='#'>
      <div class="donation-buttons2 text-center">  
  
   <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="twd00026@gmail.com">
<input type="hidden" name="lc" value="VE">
<input type="hidden" name="item_name" value="tecnocracia digital">
<input type="hidden" name="no_note" value="0">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
</form>

</div>

      </a>
    </li>
    
  </ul>
</div>

    
    <div class='course-mainbar' style='display: block;'>
      
  <h2 class='section-title'>
  Contenido : {{$curso->nombre}}
</h2>
<div class='next-lecture-wrapper'>
  <a class='btn btn-primary start-next-lecture ' data-no-turbolinks='true'>Proximo capítulo&nbsp;&nbsp;&#8250;</a>
  <span class=' hidden-sm'>
  @if($proximo_capitulo)
  {{$proximo_capitulo->nombre}}
  @endif
  </span>
</div>

<!-- Lecture list on courses page (enrolled user) -->

@foreach($curso->contenidos as $contenido)

<div class='row'>
  <div class='col-sm-12 course-section'>
    <div class='section-title' data-days-until-dripped="0" data-course-id="61310">
      <span class="section-lock">
        <i class="fa fa-lock"></i>&nbsp;
      </span>
      {{$contenido->titulo}}
      <div class="section-days-to-drip">
        <div class="section-days-logged-in">
          Available in
          <span class="section-days-to-drip-number"></span>
          days
        </div>
        <div class="section-days-logged-out">
          <span class="section-days-to-drip-number"></span>
          days
          after you enroll
        </div>
      </div>
    </div>
    <ul class='section-list'>
      @foreach($curso-> capitulos as $capitulo)

      @if($contenido->id == $capitulo->contenido_id)

      <li class='section-item incomplete' data-lecture-id="919122">
        <span class='item' data-no-turbolinks='true' >
          <span class='status-container'>
            <span class='status-icon'>
              &nbsp;
            </span>
          </span>
          <div class='title-container'>
          @if($capitulo->status == 'activo')
            <div class='btn-primary btn-sm pull-right'>
           <a href="{{$capitulo->link}}"  style="color: white;" target="_blank">Descargar</a>
            </div>
            <span class='lecture-icon'>
              <i class='fa fa-youtube-play'></i>
            </span>

          @else   
           <div class='btn-primary btn-sm pull-right lecture-start'>
           Pendiente
            </div>
            <span class='lecture-icon'>
              <i class='fa fa-youtube-play'></i>
            </span> 
           @endif
            
  
           
              {{$capitulo->nombre}}
              
              
            <span class='lecture-name'>

            </span>
          </div>
        </span>
      </li>

      @endif
      
     @endforeach
      
    </ul>

  </div>
</div>

@endforeach


<br>

<div id="disqus_thread"></div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = '//tecnocraciadigital.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                



    </div>


@endsection
