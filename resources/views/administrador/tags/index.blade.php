@extends('layouts.app')

@section('title','Tags')

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">
							
							<a href="{{route('usuarios.index')}}" class="breadcrumb-item">/ Usuarios</a>
							<a href="{{route('categorias.index')}}" class="breadcrumb-item">/ Categorias</a>
							<a href="{{route('cursos.index')}}" class="breadcrumb-item">/ Cursos</a>
							<a href="{{route('administrador.imagenes.index')}}" class="breadcrumb-item">/ Imagenes</a>
							<span class="breadcrumb-item active">/ Tags</span>
							
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

	<div class="row">
	<div class="col-md-4 col-xs-12">
		<h4>Tags registrados</h4>
	</div>

      <!-- CAJA DE BUSQUEDAS -->

      {!!Form::open(['route' => 'tags.index', 'method' => 'GET' , 'class' => 'col-lg-4 col-md-4 col-xs-12 pull-right'])!!}

     
       
            <div class="input-group">

			{!!Form::text('nombre' ,null ,['class' => 'form-control search input-lg', 'placeholder' => 'Buscar Tag'])!!}
			
            <span class="input-group-btn">
            
              <button id="search-course-button" class="btn search btn-default btn-lg" type="submit"><i class='fa fa-search'></i></button>
           
            </span>
          </div>
      
     

      {!!Form::close()!!}

      </div>
    
   <!-- CAJA DE BUSQUEDAS -->

<table class="table table-striped">	
		<thead>
			<th>ID</th>
			<th>Nombre</th>
			<th>Accion</th>
		</thead>
		<tbody>
			@foreach($tags as $tag)
				<tr>
					<td>{{$tag->id}}</td>
					<td>{{$tag->nombre}}</td>
					<td>
						<a class="btn btn-warning" href="{{route('tags.edit',$tag->id)}}"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
						<a class="btn btn-danger" href="{{route('administrador.tags.destroy',$tag->id)}}" onclick="return confirm('¿Seguro que deeas Eliminar este usuario')"><i class="fa fa-trash" aria-hidden="true"></i></a>

					</td>
				</tr>
			@endforeach
		</tbody>
</table>

<a class="btn btn-primary" href="{{route('tags.create')}}">Crear nuevo tag</a>

<div class="text-center" >{!!$tags->links()!!}</div>


</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection
