@extends('layouts.app')

@section('title','Crear Usuario')

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">

							<a href="{{route('usuarios.index')}}" class="breadcrumb-item">Usuarios</a>
							<span class="breadcrumb-item active">/ Creacion de usuarios</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Crear nuevo usuario</h4>

{!!Form::open(['route' => 'usuarios.store', 'method' => 'POST'])!!}

<div class="form-group">
	{!!Form::label('name','Nombre')!!}
	{!!Form::text('name', null, ['class' => 'form-control', 'placeholder'=> 'Nombre de usuario' ,'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('email','Correo electrónico')!!}
	{!!Form::email('email', null, ['class' => 'form-control', 'placeholder'=> 'example@correo.com' ,'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('password','Contraseña')!!}
	{!!Form::password('password', ['class' => 'form-control'  , 'placeholder'=> '*****************' ,'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('type','Tipo')!!}
	{!!Form::select('type', ['miembro' => 'Miembro','administrador' => 'Administrador'],null,['class' => 'form-control', 'placeholder' => 'Seleccione una opción...', 'required'])!!}
</div>

<div class="form-group text-center">
	{!!Form::submit('Registrar', ['class' => 'btn btn-primary '])!!}
</div>

{!!Form::close()!!}

</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection
