@extends('layouts.app')

@section('title','Usuarios')

@section('header')

@include('partials.header')

@endsection

@section('content')


<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">
							<span class="breadcrumb-item active">Usuarios</span>
							<a href="{{route('categorias.index')}}" class="breadcrumb-item">/ Categorias</a>
							<a href="{{route('cursos.index')}}" class="breadcrumb-item">/ Cursos</a>
							<a href="{{route('administrador.imagenes.index')}}" class="breadcrumb-item">/ Imagenes</a>
							<a href="{{route('tags.index')}}" class="breadcrumb-item">/ Tags</a>
							
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Usuarios registrados</h4>

<table class="table table-striped">	
		<thead>
			<th>ID</th>
			<th>Nombre</th>
			<th>Email</th>
			<th>Tipo</th>
			<th>Accion</th>
		</thead>
		<tbody>
			@foreach($usuarios as $usuario)
				<tr>
					<td>{{$usuario->id}}</td>
					<td>{{$usuario->name}}</td>
					<td>{{$usuario->email}}</td>
					<td>
						@if($usuario->type == "administrador")
							<span class="label label-danger">{{$usuario->type}}</span>
						@else
							<span class="label label-primary">{{$usuario->type}}</span>
						@endif
					</td>
					<td>
						<a class="btn btn-warning" href="{{route('usuarios.edit',$usuario->id)}}"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
						<a class="btn btn-danger" href="{{route('administrador.usuarios.destroy',$usuario->id)}}" onclick="return confirm('¿Seguro que deeas Eliminar este usuario')"><i class="fa fa-trash" aria-hidden="true"></i></a>

					</td>
				</tr>
			@endforeach
		</tbody>
</table>

<a class="btn btn-primary" href="{{url('/administrador/usuarios/create')}}">Crear nuevo usuario</a>

<div class="text-center" >{!!$usuarios->links()!!}</div>


</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection
