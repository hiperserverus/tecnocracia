@extends('layouts.app')

@section('title','Editar Usuario')

@section('header')

@include('partials.header')

@endsection

@section('content')


<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">

							<a href="{{route('usuarios.index')}}" class="breadcrumb-item">Usuarios</a>
							<span class="breadcrumb-item active">/ Edicion de usuarios</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Editar Usuario: {{$usuario->name}}</h4>

{!!Form::open(['route' => ['usuarios.update',$usuario], 'method' => 'PUT'])!!}

<div class="form-group">
	{!!Form::label('name','Nombre')!!}
	{!!Form::text('name', $usuario->name, ['class' => 'form-control', 'placeholder'=> 'Nombre de usuario' ,'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('email','Correo electrónico')!!}
	{!!Form::email('email', $usuario->email, ['class' => 'form-control', 'placeholder'=> 'example@correo.com' ,'required'])!!}
</div>


<div class="form-group">
	{!!Form::label('type','Tipo')!!}
	{!!Form::select('type', ['miembro' => 'Miembro','administrador' => 'Administrador'],$usuario->type,['class' => 'form-control'])!!}
</div>

<div class="form-group text-center">
	{!!Form::submit('Editar', ['class' => 'btn btn-primary '])!!}
</div>

{!!Form::close()!!}

</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection
