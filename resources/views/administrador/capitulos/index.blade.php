@extends('layouts.app')

@section('title','Capitulos')

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">
							<a href="{{route('cursos.index')}}" class="breadcrumb-item">Cursos</a>
							<a href="{{route('contenidos.index',$curso_id)}}" class="breadcrumb-item">/ Contenidos</a>
							<span class="breadcrumb-item active">/ Capitulos del contenido</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<div class="row">
	<div class="col-md-4 col-xs-12">
		<h4>Capitulos registrados</h4>
	</div>

 <!-- CAJA DE BUSQUEDAS -->

      {!!Form::open(['route' => ['capitulos.index',$curso_id,$contenido_id], 'method' => 'GET' , 'class' => 'col-lg-4 col-md-4 col-xs-12 pull-right'])!!}

     
       
            <div class="input-group">

			{!!Form::text('link' ,null ,['class' => 'form-control search input-lg', 'placeholder' => 'Buscar Link'])!!}
			
            <span class="input-group-btn">
            
              <button id="search-course-button" class="btn search btn-default btn-lg" type="submit"><i class='fa fa-search'></i></button>
           
            </span>
          </div>
      
     

      {!!Form::close()!!}

      </div>
    
   <!-- CAJA DE BUSQUEDAS -->


<table class="table table-striped">	
		<thead>
			<th>ID</th>
			<th>Nombre del Capitulo</th>
			<th>Link de Descarga</th>
			<th>Estado del Link</th>
			<th>Accion</th>
		</thead>
		<tbody>
			@foreach($capitulos as $capitulo)
				<tr>
					<td>{{$capitulo->id}}</td>
					<td>{{$capitulo->nombre}}</td>
					<td><a href="{{$capitulo->link}}" target="_blank">{{$capitulo->link}}</a></td>
					<td>
						
						@if($capitulo->status == "activo")
							<span class="label label-primary">{{$capitulo->status}}</span>
						@else
							<span class="label label-danger">{{$capitulo->status}}</span>
						@endif


					</td>
					<td>
						<a class="btn btn-warning" href="{{route('capitulos.edit',[$curso_id,$capitulo->contenido_id,$capitulo->id])}}"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
						<a class="btn btn-danger" href="{{route('administrador.capitulos.destroy',[$curso_id,$capitulo->contenido_id,$capitulo->id])}}" onclick="return confirm('¿Seguro que deeas Eliminar este usuario')"><i class="fa fa-trash" aria-hidden="true"></i></a>

					</td>
				</tr>
			@endforeach
		</tbody>
</table>

<a class="btn btn-primary" href="{{route('capitulos.create',[$curso_id,$contenido_id])}}">Crear nuevo capitulo</a>

<div class="text-center" >{!!$capitulos->links()!!}</div>


</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection