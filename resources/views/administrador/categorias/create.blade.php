@extends('layouts.app')

@section('title','Crear Categoria')

@section('header')

@include('partials.header')

@endsection

@section('content')


<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">

							<a href="{{route('categorias.index')}}" class="breadcrumb-item">Categorias</a>
							<span class="breadcrumb-item active">/ Creacion de categorias</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Crear nueva categoria</h4>

{!!Form::open(['route' => 'categorias.store', 'method' => 'POST'])!!}

<div class="form-group">
	{!!Form::label('nombre','Nombre')!!}
	{!!Form::text('nombre', null, ['class' => 'form-control', 'placeholder'=> 'Nombre de la categoria' ,'required'])!!}
</div>


<div class="form-group text-center">
	{!!Form::submit('Registrar', ['class' => 'btn btn-primary '])!!}
</div>

{!!Form::close()!!}

</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection