@extends('layouts.app')

@section('title','Editar Contenido')

@section('header')

@include('partials.header')

@endsection

@section('content')


<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">

							<a href="{{route('cursos.index')}}" class="breadcrumb-item">Cursos</a>
							<a href="{{route('contenidos.index',$contenido->curso_id)}}" class="breadcrumb-item">/ Contenidos</a>
							<span class="breadcrumb-item active">/ Edicion de contenidos</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Editar Contenido: ("{{$contenido->titulo}}")</h4>

{!!Form::open(['route' => ['contenidos.update',$contenido->curso_id,$contenido->id], 'method' => 'PUT'])!!}

<div class="form-group">
	{!!Form::label('titulo','Titulo del contenido')!!}
	{!!Form::text('titulo', $contenido->titulo, ['class' => 'form-control', 'placeholder'=> 'Titulo del contenido' ,'required'])!!}
</div>


<div class="form-group text-center">
	{!!Form::submit('Editar', ['class' => 'btn btn-primary '])!!}
</div>

{!!Form::close()!!}

</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection