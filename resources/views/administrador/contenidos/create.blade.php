@extends('layouts.app')

@section('title','Crear Categoria')

@section('header')

@include('partials.header')

@endsection

@section('content')


<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">

							<a href="{{route('cursos.index')}}" class="breadcrumb-item">Cursos</a>
							<a href="{{route('contenidos.index',$curso_id)}}" class="breadcrumb-item">/ Contenidos</a>
							<span class="breadcrumb-item active">/ Creacion de contenidos</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Crear nuevo contenido</h4>

{!!Form::open(['route' => ['contenidos.store',$curso_id], 'method' => 'POST'])!!}

<div class="form-group">
	{!!Form::label('titulo','Titulo del contenido')!!}
	{!!Form::text('titulo', null, ['class' => 'form-control', 'placeholder'=> 'Titulo del contenido' ,'required'])!!}
</div>


<div class="form-group text-center">
	{!!Form::submit('Registrar', ['class' => 'btn btn-primary '])!!}
</div>

{!!Form::close()!!}

</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection