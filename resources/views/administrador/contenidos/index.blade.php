@extends('layouts.app')

@section('title','Contenidos')

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">

							<a href="{{route('cursos.index')}}" class="breadcrumb-item">Cursos</a>
							<span class="breadcrumb-item active">/ Contenido del curso</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Contenido del curso</h4>

<table class="table table-striped">	
		<thead>
			<th>ID</th>
			<th>Titulo del Contenido</th>
			<th>Accion</th>
		</thead>
		<tbody>
			@foreach($contenidos as $contenido)
				<tr>
					<td>{{$contenido->id}}</td>
					<td><a href="{{route('capitulos.index',[$contenido->curso_id,$contenido->id])}}">{{$contenido->titulo}}</a></td>
					<td>
						<a class="btn btn-warning" href="{{route('contenidos.edit',[$contenido->curso_id,$contenido->id])}}"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
						<a class="btn btn-danger" href="{{route('administrador.contenidos.destroy',[$contenido->curso_id,$contenido->id])}}" onclick="return confirm('¿Seguro que deeas Eliminar este usuario')"><i class="fa fa-trash" aria-hidden="true"></i></a>

					</td>
				</tr>
			@endforeach
		</tbody>
</table>

				<a class="btn btn-primary" href="{{route('contenidos.create',[$curso_id])}}">Crear nuevo contenido</a>

			<div class="text-center" >{!!$contenidos->links()!!}</div>
</div>
</div>


@endsection

@section('footer')

@include('partials.footer')

@endsection