@extends('layouts.app')

@section('title','políticas de privacidad')

@section('header')

@include('partials.header_content')

@endsection

@section('content')

<div class="view-school">

  <div class="blocks-page" id="blocks-page-90634">
      
        
          <div class="course-block block rich_text " id="block-1064839">
            <div class='container'>
  <div class='row'>
    <div class='col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 course-description'>
      <h2>Política de privacidad</h2>Esta Política de Privacidad rige la manera en la que el sitio web recopila, utiliza, mantiene y divulga la información recogida de los usuarios (cada uno, un "Usuario") del sitio web ( "Sitio"). Esta política de privacidad se aplica al sitio y todos los productos y servicios ofrecidos por la compañía. <br><br><b>Información de identificación personal</b><br><br>Podemos recopilar información de identificación personal de los usuarios en una variedad de maneras, incluyendo, pero no limitado a, cuando los usuarios visitan nuestro sitio, registrarse en el sitio, suscribirse al boletín, y en relación con las actividades de otros, servicios, características o recursos que ponen a disposición en nuestro sitio. Los usuarios pueden pedir, como la dirección apropiada, de correo electrónico. Los usuarios pueden, sin embargo, visitar nuestro sitio de forma anónima. Vamos a recoger información de identificación personal de los usuarios sólo si se someten voluntariamente esa información a nosotros. Los usuarios siempre pueden negarse a suministrar información de identificación personal, excepto que puede evitar que la participación en determinadas actividades relacionadas con el sitio. <br><br><b>Información de identificación no personal</b><br><br>Podemos recopilar información de identificación no personal sobre los usuarios cuando interactúan con nuestro sitio. Información de identificación no personal puede incluir el nombre del navegador, el tipo de equipo e información técnica sobre los usuarios mediante la conexión a nuestro sitio, tales como el sistema operativo y los proveedores de servicios de Internet utilizados y otra información similar. <br><br><b>Las cookies del navegador Web</b><br><br>Nuestro sitio puede utilizar "cookies" para mejorar la experiencia del usuario. El navegador web del usuario coloca cookies en su disco duro para propósitos de registro y, a veces para rastrear información sobre ellos. El usuario puede optar por configurar su navegador para rechazar las cookies, o para que le avise cuando se envíen cookies. Si lo hacen, tenga en cuenta que algunas partes del sitio pueden no funcionar correctamente. <br><br><b>¿Cómo usamos la información recopilada</b><br><br>Compañía puede recopilar y utilizar los usuarios información personal para los siguientes fines:<br><ul><li><i>- Para mejorar el servicio al cliente</i><br>información que usted proporcione nos ayuda a responder a sus solicitudes de servicio al cliente y las necesidades de apoyo de manera más eficiente.</li><li><i>- Para personalizar la experiencia del usuario</i><br>Podemos utilizar la información en conjunto para comprender cómo nuestros usuarios como grupo, utilizan los servicios y recursos ofrecidos en nuestro Sitio.</li><li><i>- Para enviar correos electrónicos periódicos</i><br>Podemos utilizar la dirección de correo electrónico para enviar información de usuario y las actualizaciones correspondientes a su orden. También se puede utilizar para responder a sus consultas, preguntas, y / u otras solicitudes. Si el usuario decide optar en nuestra lista de correo, recibirán correos electrónicos que pueden incluir noticias de la compañía, actualizaciones, relacionada con el producto o servicio de información, etc. Si en cualquier momento el usuario desea dejar de recibir futuros correos electrónicos, incluimos detallada instrucciones para darse de baja en la parte inferior de cada correo electrónico o usuario podrá ponerse en contacto con nosotros a través de nuestro sitio.</li></ul><b>¿Cómo protegemos su información</b><br><br>Adoptamos prácticas de recopilación de datos, almacenamiento y procesamiento y las medidas de seguridad para proteger contra el acceso no autorizado, alteración, divulgación o destrucción de su información personal, nombre de usuario, contraseña, información de transacciones y los datos almacenados en nuestro Sitio. <br><br><b>Compartiendo su información personal</b><br><br>No vendemos, comercializamos ni alquilamos los usuarios información de identificación personal a terceros. Podemos compartir información demográfica genérica no vinculada a ninguna información de identificación personal sobre los visitantes y usuarios con nuestros socios comerciales, afiliados y anunciantes de confianza para los fines antes mencionados. <br><br><b>Sitios web de terceros</b><br><br>Los usuarios pueden encontrar publicidad o de otros contenidos en nuestro sitio que enlace a los sitios y servicios de nuestros socios, proveedores, anunciantes, patrocinadores, concesionarios y otros terceros. No tenemos control sobre el contenido o enlaces que aparecen en estos sitios y no son responsables de las prácticas empleadas por sitios web vinculados hacia o desde nuestro sitio. Además, estos sitios o servicios, incluyendo su contenido y enlaces, pueden estar cambiando constantemente. Estos sitios y servicios pueden tener sus propias políticas de privacidad y las políticas de servicio al cliente. Navegación e interacción en cualquier otro sitio web, incluyendo sitios web que tienen un enlace a nuestro sitio, está sujeto a los propios términos y políticas de ese sitio web. <br><br><b>Cambios en esta política de privacidad</b><br><br>de la empresa tiene la facultad de actualizar esta política de privacidad en cualquier momento. Cuando lo hacemos, vamos a revisar la fecha de actualización en la parte inferior de esta página. Animamos a los usuarios a comprobar con frecuencia esta página para cualquier cambio para mantenerse informado acerca de cómo estamos ayudando a proteger la información personal que recopilamos. Usted reconoce y acepta que es su responsabilidad revisar esta política de privacidad periódicamente y tomar conciencia de las modificaciones.<br><br><b>Su aceptación de estos términos</b><br><br>Al usar este sitio, usted expresa su aceptación de esta política. Si no está de acuerdo con esta política, por favor no utilice nuestro sitio. Su uso continuado del Sitio después de la publicación de cambios a esta política será considerado su aceptación de dichos cambios.<br><br>
    </div>
  </div>
</div>

          </div>
        
      
    </div>


</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection