<footer class='bottom-menu bottom-menu-inverse'>
  <div class='container'>
    <div class='row'>
      <div class='col-xs-12 col-sm-6 col-md-6'>
        <p>
          &copy;
          2017 JEGMSolutions
        </p>
      </div>
      <div class='col-xs-12 col-sm-6 col-md-6 text-right'>
        <ul class='list-unstyled'>
          <li>
            <a href="{{ url('/terminos') }}">
              Terminos de Uso
            </a>
          </li>
          <li>
            <a href="{{ url('/politicas') }}">
              Política de Privacidad
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>

