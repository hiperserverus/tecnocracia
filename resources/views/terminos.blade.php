@extends('layouts.app')

@section('title','términos y condiciones')

@section('header')

@include('partials.header_content')

@endsection

@section('content')

<div class="view-school">

     <div class="blocks-page" id="blocks-page-90635">
      
        
          <div class="course-block block rich_text " id="block-1064840">
            <div class='container'>
  <div class='row'>
    <div class='col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 course-description'>
      <h2>Términos y Condiciones de Uso del Sitio Web</h2><h3>1. Términos</h3><p>Al acceder a este sitio web, usted acepta estar obligado por estos Términos del sitio web y Condiciones de Uso, todas las leyes y reglamentos aplicables, y acepta que es responsable del cumplimiento de las leyes locales aplicables. Si no está de acuerdo con cualquiera de estos términos, está prohibido el uso o acceso a este sitio. Los materiales contenidos en este sitio web están protegidos por derechos de autor y derecho de marcas.</p><h3>2. Licencia de uso</h3><ol type='a'><li> Se concede permiso para descargar temporalmente una copia de los materiales (información o software) en el sitio web de la empresa por sólo transitoria de visión personal, no comercial. Esta es la concesión de una licencia, no una transferencia de título, y bajo esta licencia no podrá:<ol type='i'><li>modificar o copiar los materiales;</li><li>utilizar los materiales para cualquier propósito comercial, o para cualquier exhibición pública (comercial o no comercial);</li><li>intentar descompilar o realizar ingeniería inversa cualquier software contenido en el sitio web de la empresa;</li><li>eliminar cualquier derecho de autor u otros notaciones de propiedad de los materiales;</li><li>transferir el material a otra persona o "espejo" de los materiales en cualquier otro servidor.</li></ol></li><li>Esta licencia terminará automáticamente si usted viola cualquiera de estas restricciones y puede ser denunciado por la empresa en cualquier momento. Al terminar su visión de estos materiales o al término de esta licencia, deberá destruir cualquier material descargado en su poder ya sea en formato electrónico o impreso.</li></ol><h3>3. Exclusión</h3><ol type='a'><li>Los materiales en el sitio web de la empresa se proporcionan "tal cual". Company no ofrece ninguna garantía, expresa o implícita, y por la presente renuncia y niega cualquier otra garantía, incluyendo, sin limitación, las garantías o condiciones implícitas de comerciabilidad, adecuación para un propósito particular o no infracción de la propiedad intelectual o cualquier otra violación de los derechos. Además, la empresa no garantiza ni hace ninguna representación con respecto a la exactitud, los resultados probables, o la fiabilidad de la utilización de los materiales en su página de Internet o de otro tipo en relación con los materiales o en cualquier sitio ligado a este sitio.</li></ol><h3>4. Limitaciones</h3><p>En ningún caso la empresa o sus proveedores serán responsables por cualquier daño (incluyendo, sin limitación, daños por pérdida de datos o beneficios, o debido a la interrupción del negocio,) que surjan del uso o la imposibilidad de usar los materiales en el sitio Internet de la empresa, aun si la empresa o de un representante autorizado de la empresa ha sido notificado en forma oral o por escrito de la posibilidad de tales daños. Debido a que algunas jurisdicciones no permiten limitaciones en garantías implícitas o limitaciones de responsabilidad por daños consecuenciales o incidentales, estas limitaciones pueden no aplicarse en su caso.</p><h3>5. Las revisiones y errores</h3><p>Los materiales que aparecen en el sitio web de la empresa podrían incluir aspectos técnicos, tipográficos o fotográficos errores. Compañía no garantiza que cualquiera de los materiales en su sitio web sea precisa, completa o actualizada. Compañía puede realizar cambios en los materiales contenidos en su sitio web en cualquier momento y sin previo aviso. Empresa no significa, sin embargo, que se compromete a actualizar los materiales.</p><h3>6. Enlaces</h3><p>Empresa no ha revisado todos los sitios vinculados a su página de Internet y no es responsable de los contenidos de dicho sitio. La inclusión de estos vínculos no implica aprobación por parte de la empresa del sitio. El uso de cualquier sitio web vinculado como es bajo el propio riesgo del usuario.</p><h3>7. Términos de Uso Modificaciones</h3><p>Compañía puede revisar estos términos de uso de su sitio web en cualquier momento y sin previo aviso. Mediante el uso de este sitio web, usted acepta que quedará vinculado por la versión actual de estos Términos y Condiciones de Uso.</p><h3>8. Ley de Gobierno</h3><p>Cualquier reclamación relacionada con el sitio web de la empresa se regirá por las leyes del Estado de Nueva York, sin consideración a su conflicto de disposiciones legales.</p>
    </div>
  </div>
</div>

          </div>
        
      
    </div>


</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection