<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contenido extends Model
{
    
    protected $table = "contenidos";
    protected $fillable = ['titulo','curso_id'];

        public function curso() {

    	return $this->belongsTo('App\Curso','curso_id');
    }

       public function scopeBuscarContenido ($query, $curso_id) {

    	return $query->where('curso_id','=',$curso_id);
    }

            public function capitulos () {

        return $this->hasMany('App\Capitulo','contenido_id');
    }

}
