<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    protected $table = "imagenes";
    protected $fillable = ['nombre','curso_id'];


    public function curso() {

    	return $this->belongsTo('App\Curso');
    }
}


