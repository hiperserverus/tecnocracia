<?php


namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Categoria;
use App\User;

class AsideComposer {

	public function compose(View $view) {

		$categorias = Categoria::orderBy('nombre','DESC')->get();
		$usuarios = User::orderBy('name','DESC')->get();

		

		$view->with(['categorias' => $categorias, 'usuarios' => $usuarios]);
	}

}