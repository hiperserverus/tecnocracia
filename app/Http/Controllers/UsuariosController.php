<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Laracasts\Flash\Flash;
use App\Http\Requests\UsuarioRequest;
class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::orderBy('id','ASC')->paginate(10);

        return view('administrador.usuarios.index',['usuarios'=>$usuarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuarioRequest $request)
    {
        
        $usuario = new User($request->all());
        $usuario->password = bcrypt($request->password);
        

       if($usuario->save()) {

        Flash::success("Se ha registrado el usuario: ". $usuario->name . " Exitosamente¡¡");
        return redirect()->route('usuarios.index');     
       
       }else{
        Flash::error("No se pudo crear el usuario");
        return redirect()->route('usuarios.create');   
       }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $usuario = User::find($id);

       return view('administrador.usuarios.edit',['usuario' => $usuario]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = User::find($id);

        $usuario->fill($request->all());
        
        if($usuario->save()) {

        Flash::success("Usuario: " . $usuario->name . " Actualizado Exitosamente¡¡");
        return redirect()->route('usuarios.index');     
       
       }else{
        Flash::error('No se pudo actualizar el usuario');
        return redirect()->route('usuarios.edit',$id); 
       }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::find($id);
        
        if($usuario->delete()) {

        Flash::success("Usuario: " . $usuario->name . " Eliminado Exitosamente¡¡");
        return redirect()->route('usuarios.index');    
       
       }else{
        Flash::error('No se pudo eliminar el usuario');
        return redirect()->route('usuarios.index');  
       }

    }
}
