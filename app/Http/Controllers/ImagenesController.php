<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imagen;

class ImagenesController extends Controller
{
   public function index(){

   	$imagenes = Imagen::orderBy('id','DESC')->simplepaginate(6);

   	$imagenes->each(function ($imagenes){
   		$imagenes->curso;
   	});
   	

   	return view('administrador.imagenes.index',['imagenes' => $imagenes]);

   }
}
